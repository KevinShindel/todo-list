export class Model {
    constructor(public user = 'Adam',
                public items = [
                    new TodoItem('Buy Flowers', false),
                    new TodoItem('Get Shoes', true),
                    new TodoItem('Collect Tickets', false),
                    new TodoItem('Call Joe', false),
                ]) {
    }
}

export class TodoItem {
    constructor(public action: string, public done: boolean) {
    }
}
