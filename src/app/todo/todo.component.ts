import { Component, OnInit } from '@angular/core';
import {Model, TodoItem} from "../../models/model";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  model = new Model();

  constructor() { }

  ngOnInit(): void {
  }

  getName() {
    return this.model.user;
  }

  getTodoItems() {
    return this.model.items;
  }

  addItem(value: string) {
    this.model.items.push(new TodoItem(value, false));
  }

}
